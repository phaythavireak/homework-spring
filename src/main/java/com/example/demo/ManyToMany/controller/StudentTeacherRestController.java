package com.example.demo.ManyToMany.controller;


import com.example.demo.ManyToMany.model.Students;
import com.example.demo.ManyToMany.model.Teacher;
import com.example.demo.ManyToMany.stduent_teacherentity.Student_TeacherEntity;
import com.example.demo.OneToManyAndManytoOne.model.Account;
import com.example.demo.OneToManyAndManytoOne.model.Department;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import javax.persistence.ManyToMany;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@RestController
@RequestMapping("/api/v1/manytomany")
public class StudentTeacherRestController {




    @Autowired
    private Student_TeacherEntity student_teacherEntity;


    @GetMapping("/all")
    public Map<String,Object> getalllist()
    {

        Map<String,Object> map = new HashMap<>();
        List<Students> students = student_teacherEntity.getallList();
        if(!students.isEmpty())
        {
            map.put("message","Data Found");
            map.put("status",true);
            map.put("Data",students);

        }
        else
        {
            map.put("message","Data not Found");
            map.put("status",false);
        }
        return  map;




    }
    @PostMapping("/save")
    public Map<String,Object> save()
    {
        Map<String,Object> map = new HashMap<>();

        Students students = new Students();
        Teacher teacher = new Teacher();
        teacher.setName("virak");
        teacher.setPhone_number(12343);
        students.setStudent_name("phay");
        students.setTeacher(Collections.singletonList(teacher));
        Students students1 = student_teacherEntity.insertes(students);
        if(students1 != null)
        {
            map.put("message","Data Found");
            map.put("status",true);
            map.put("Data",students1);

        }
        else
        {
            map.put("message","Data not Found");
            map.put("status",false);
        }
        return  map;
    }
    @DeleteMapping("/delete/{id}")
    public Map<String,Object> delete(@PathVariable Integer id)
    {
        Map<String,Object> map = new HashMap<>();
        student_teacherEntity.deletebyid(id);
        map.put("message","Data Found");
        map.put("status",true);



        return  map;
    }
    @PutMapping("/update")
    public Map<String,Object> update()
    {
        Map<String,Object> map = new HashMap<>();

        Students students = new Students();
        Teacher teacher = new Teacher();
        teacher.setName("sdach");
        teacher.setPhone_number(12);
        students.setStudent_name("game");
        students.setTeacher(Collections.singletonList(teacher));
        Integer student12 = student_teacherEntity.findid(1).getId();
        students.setId(student12);
        if(student_teacherEntity.updatea(students) != null)
        {
            map.put("message","Data Found");
            map.put("status",true);
            map.put("Data",students);
        }
        else
        {
            map.put("message","Data Found");
            map.put("status",false);

        }
        return  map;
    }













}
