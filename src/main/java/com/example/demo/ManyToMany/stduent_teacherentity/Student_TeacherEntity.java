package com.example.demo.ManyToMany.stduent_teacherentity;


import com.example.demo.ManyToMany.model.Students;
import com.example.demo.OneToManyAndManytoOne.model.Account;
import com.example.demo.OneToOne.model.Student;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import java.util.List;

@Service
@Transactional
public class Student_TeacherEntity {


    @PersistenceContext
    private EntityManager entityManager;



    public Students insertes(Students students)
    {
        entityManager.persist(students);
        return students;
    }
    public List getallList()
    {
        return entityManager.createQuery("select ste from studentes_entity ste").getResultList();
    }



    public Students findid(Integer id)
    {
       return   entityManager.find(Students.class,id);
    }

    public void deletebyid(Integer id)
    {
         Students students = findid(id);
        entityManager.remove(students);

    }
    public Students updatea(Students students)
    {
        return entityManager.merge(students);
    }
}
