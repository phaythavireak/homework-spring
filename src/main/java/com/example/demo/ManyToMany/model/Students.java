package com.example.demo.ManyToMany.model;


import com.fasterxml.jackson.annotation.JsonBackReference;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonManagedReference;

import javax.persistence.*;
import java.util.List;

@Entity(name = "studentes_entity")
@Table(name = "tbl_studentes")
public class Students {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private int id;
    private String student_name;
    @ManyToMany(targetEntity = Teacher.class,cascade = CascadeType.ALL,fetch = FetchType.LAZY)
    @JsonIgnore
    public List teacher;

    @Override
    public String toString() {
        return "Students{" +
                "id=" + id +
                ", student_name='" + student_name + '\'' +
                '}';
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getStudent_name() {
        return student_name;
    }

    public void setStudent_name(String student_name) {
        this.student_name = student_name;
    }

    public List getTeacher() {
        return teacher;
    }

    public void setTeacher(List teacher) {
        this.teacher = teacher;
    }

    public Students() {

    }

    public Students(String student_name, List teacher) {

        this.student_name = student_name;
        this.teacher = teacher;
    }
}
