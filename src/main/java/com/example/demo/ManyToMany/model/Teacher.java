package com.example.demo.ManyToMany.model;


import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonManagedReference;

import javax.persistence.*;
import java.util.List;

@Entity(name = "teacher_entity")
@Table(name = "tbl_teacher")
public class Teacher {


    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private int id;
    private String name;



    private int phone_number;

    @Override
    public String toString() {
        return "Teacher{" +
                "id=" + id +
                ", name='" + name + '\'' +
                ", phone_number=" + phone_number +
                '}';
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getPhone_number() {
        return phone_number;
    }

    public void setPhone_number(int phone_number) {
        this.phone_number = phone_number;
    }

    public Teacher() {

    }

    public Teacher(String name, int phone_number) {

        this.name = name;
        this.phone_number = phone_number;
    }
}
