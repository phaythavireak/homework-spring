package com.example.demo.OneToOne.model;


import javax.persistence.*;

@Entity(name = "student_entity")
@Table(name = "tbl_student")
public class Student {
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    @Column(name = "stu_id")
    private  Integer id;
    @Column(name = "stu_name")
    private String name;
    @Column(name = "stu_address")
    private String address;
    @Column(name = "stu_phone")
    private Integer phone;
    @OneToOne(cascade = CascadeType.ALL,orphanRemoval = true)
    @JoinColumn(name = "course_id")
    private Course course;

    @Override
    public String toString() {
        return "Department{" +
                "id=" + id +
                ", name='" + name + '\'' +
                ", address='" + address + '\'' +
                ", phone=" + phone +
                ", course=" + course +
                '}';
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public Integer getPhone() {
        return phone;
    }

    public void setPhone(Integer phone) {
        this.phone = phone;
    }

    public Course getCourse() {
        return course;
    }

    public void setCourse(Course course) {
        this.course = course;
    }

    public Student() {

    }

    public Student(String name, String address, Integer phone, Course course) {
        this.name = name;
        this.address = address;
        this.phone = phone;
        this.course = course;
    }
}
