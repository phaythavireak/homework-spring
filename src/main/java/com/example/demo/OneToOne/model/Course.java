package com.example.demo.OneToOne.model;


import javax.persistence.*;

@Entity(name = "course_entity")
@Table(name = "tbl_course")
public class Course {


    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    @Column(name = "course_id")
    private  Integer id;
    @Column(name = "course_title")
    private String title;
    @Column(name = "course_description")
    private String description;

    @Override
    public String toString() {
        return "Account{" +
                "id=" + id +
                ", title='" + title + '\'' +
                ", description='" + description + '\'' +
                '}';
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public Course() {

    }

    public Course(String title, String description) {

        this.title = title;
        this.description = description;
    }
}
