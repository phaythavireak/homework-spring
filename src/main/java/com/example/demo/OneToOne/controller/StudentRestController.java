package com.example.demo.OneToOne.controller;


import com.example.demo.OneToOne.entitiy.StudentEntity;
import com.example.demo.OneToOne.model.Course;
import com.example.demo.OneToOne.model.Student;
import org.springframework.web.bind.annotation.*;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

@RestController
@RequestMapping("/api/v1")
public class StudentRestController {


    private StudentEntity studentEntity;

    public StudentRestController(StudentEntity studentEntity) {
        this.studentEntity = studentEntity;
    }

    @GetMapping("/all")
    public Map<String,Object> getall(Student student)
    {
        Map<String,Object> map = new HashMap<>();
        List<Student> students = studentEntity.getall(student);
        if(!students.isEmpty())
        {
            map.put("message","Data Found");
            map.put("status",true);
            map.put("Data",students);

        }
        else
        {
            map.put("message","Data not Found");
            map.put("status",false);
        }
        return  map;
    }

    @PostMapping("/save")
    public Map<String,Object> save()
    {
        Map<String,Object> map = new HashMap<>();

        Student student1 = new Student();
        Course course1 = new Course();
        course1.setTitle("mazer");
        course1.setDescription("Blockchain");
        student1.setName("som nak");
        student1.setPhone(12345679);
        student1.setAddress("VN");
        student1.setCourse(course1);
        Student student2 = studentEntity.save(student1);
        if(student2 != null)
        {
            map.put("message","Data Found");
            map.put("status",true);
            map.put("Data",student2);

        }
        else
        {
            map.put("message","Data not Found");
            map.put("status",false);
        }
        return  map;
    }
    @DeleteMapping("/delete/{id}")
    public Map<String,Object> delete(@PathVariable Integer id)
    {
        Map<String,Object> map = new HashMap<>();
            studentEntity.remove(id);
            map.put("message","Data Found");
            map.put("status",true);



        return  map;
    }
    @PutMapping("/update")
    public Map<String,Object> update()
    {
        Map<String,Object> map = new HashMap<>();
        Student student23 = new Student();
        Course course23 = new Course();
        course23.setTitle("I");
        course23.setDescription("S");
        student23.setName("v");
        student23.setAddress("PP");
        student23.setPhone(12345678);
        student23.setCourse(course23);
        Integer student12 = studentEntity.find(1).getId();
        student23.setId(student12);
        if(studentEntity.update(student23) != null)
        {
            map.put("message","Data Found");
            map.put("status",true);
            map.put("Data",student23);
        }
        else
            {
                map.put("message","Data Found");
                map.put("status",false);

        }
        return  map;
    }


}
