package com.example.demo.OneToOne.entitiy;


import com.example.demo.OneToOne.model.Student;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import java.util.List;

@Service
@Transactional
public class StudentEntity {


    @PersistenceContext
    private EntityManager entityManager;


    public Student update(Student student)
    {
        return entityManager.merge(student);
    }

    public Student save(Student student) {
        entityManager.persist(student);
        return student;
    }
    public Student find(Integer id) {
        return entityManager.find(Student.class, id);
    }

    public void remove(Integer id)
    {
        Student student = find(id);
        entityManager.remove(student);

    }
    public List getall(Student student)
    {
        return entityManager.createQuery("select se from student_entity se").getResultList();
    }



}
