package com.example.demo.OneToManyAndManytoOne.controller;


import com.example.demo.OneToManyAndManytoOne.entitiy.AccountEntity;
import com.example.demo.OneToManyAndManytoOne.model.Account;
import com.example.demo.OneToManyAndManytoOne.model.Department;
import org.springframework.web.bind.annotation.*;

import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@RestController
@RequestMapping("/api/v1/onetomany")
public class AccountRestController {

   private AccountEntity accountEntity;

    public AccountRestController(AccountEntity departmentsEntity) {
        this.accountEntity = departmentsEntity;
    }

    @GetMapping("/all")
    public Map<String,Object> getall()
    {
        Map<String,Object> map = new HashMap<>();
        List<Account> departments = accountEntity.get();
        if(!departments.isEmpty())
        {
            map.put("message","Data Found");
            map.put("status",true);
            map.put("Data",departments);

        }
        else
        {
            map.put("message","Data not Found");
            map.put("status",false);
        }
        return  map;
    }

    @PostMapping("/save")
    public Map<String,Object> save()
    {
        Map<String,Object> map = new HashMap<>();

        Department department11 = new Department();
        Account account = new Account();
        account.setAccount_name("ACE");
        account.setAccount_num(324);
        department11.setName("crush");
        department11.setPhone(01023);
        department11.setAddress("THAI");
        account.setDepartment(department11);
        department11.setAccounts(Collections.singletonList(account));
        Account account12 = accountEntity.insert(account);
        if(account12 != null)
        {
            map.put("message","Data Found");
            map.put("status",true);
            map.put("Data",account12);

        }
        else
        {
            map.put("message","Data not Found");
            map.put("status",false);
        }
        return  map;
    }
    @DeleteMapping("/delete/{id}")
    public Map<String,Object> delete(@PathVariable Integer id)
    {
        Map<String,Object> map = new HashMap<>();
        accountEntity.delete(id);
            map.put("message","Data Found");
            map.put("status",true);



        return  map;
    }
    @PutMapping("/update")
    public Map<String,Object> update()
    {
        Map<String,Object> map = new HashMap<>();
        Account account = new Account();
        Department department = new Department();
        account.setAccount_num(1234);
        account.setAccount_name("rady");
        account.setDepartment(department);
        department.setPhone(4321);
        department.setAddress("youtube");
        department.setName("blockchain");
        Integer student12 = accountEntity.findbyid(1).getId();
        account.setId(student12);
        if(accountEntity.updatea(account) != null)
        {
            map.put("message","Data Found");
            map.put("status",true);
            map.put("Data",account);
        }
        else
            {
                map.put("message","Data Found");
                map.put("status",false);

        }
        return  map;
    }


}
