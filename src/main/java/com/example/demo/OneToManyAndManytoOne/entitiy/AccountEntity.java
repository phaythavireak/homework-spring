package com.example.demo.OneToManyAndManytoOne.entitiy;


import com.example.demo.OneToManyAndManytoOne.model.Account;
import com.example.demo.OneToOne.model.Student;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import java.util.List;

@Service
@Transactional
public class AccountEntity {



    @PersistenceContext
    private EntityManager entityManager;




    public List get()
    {
        return entityManager.createQuery("select de from account_entity de ").getResultList();
    }
    public Account insert(Account account)
    {
         entityManager.persist(account);
         return account;
    }


    public Account findbyid(Integer id) {
        return entityManager.find(Account.class, id);
    }

    public void delete(Integer id)
    {
        Account account = findbyid(id);
        entityManager.remove(account);

    }
    public Account updatea(Account account)
    {
        return entityManager.merge(account);
    }
}
