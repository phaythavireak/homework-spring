package com.example.demo.OneToManyAndManytoOne.model;


import com.fasterxml.jackson.annotation.JsonManagedReference;

import javax.persistence.*;

@Entity(name = "account_entity")
@Table(name = "tbl_account")
public class Account {


    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    @Column(name = "account_id")
    private  Integer id;
    @Column(name = "account_number")
    private Integer account_num;
    @Column(name = "account_name")
    private String account_name;
    @ManyToOne(cascade = CascadeType.ALL)
    @JoinColumn(name = "department_id")
    @JsonManagedReference
    private Department department;

    @Override
    public String toString() {
        return "Account{" +
                "id=" + id +
                ", account_num=" + account_num +
                ", account_name='" + account_name + '\'' +
                '}';
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public Integer getAccount_num() {
        return account_num;
    }

    public void setAccount_num(Integer account_num) {
        this.account_num = account_num;
    }

    public String getAccount_name() {
        return account_name;
    }

    public void setAccount_name(String account_name) {
        this.account_name = account_name;
    }

    public Department getDepartment() {
        return department;
    }

    public void setDepartment(Department department) {
        this.department = department;
    }

    public Account() {

    }

    public Account(Integer account_num, String account_name, Department department) {
        this.account_num = account_num;
        this.account_name = account_name;
        this.department = department;
    }
}
