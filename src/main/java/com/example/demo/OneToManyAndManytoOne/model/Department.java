package com.example.demo.OneToManyAndManytoOne.model;


import com.fasterxml.jackson.annotation.JsonBackReference;
import com.fasterxml.jackson.annotation.JsonManagedReference;

import javax.persistence.*;
import java.util.List;

@Entity(name = "department_entity")
@Table(name = "tbl_department")
public class Department {
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    @Column(name = "department_id")
    private  Integer id;
    @Column(name = "department_name")
    private String name;
    @Column(name = "department_address")
    private String address;
    @Column(name = "department_phone")
    private Integer phone;
    @OneToMany(mappedBy = "department",targetEntity = Account.class,cascade = CascadeType.ALL,orphanRemoval = true)
    @JsonBackReference
    private List<Account> accounts;

    @Override
    public String toString() {
        return "Department{" +
                "id=" + id +
                ", name='" + name + '\'' +
                ", address='" + address + '\'' +
                ", phone=" + phone +
                '}';
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public Integer getPhone() {
        return phone;
    }

    public void setPhone(Integer phone) {
        this.phone = phone;
    }

    public List<Account> getAccounts() {
        return accounts;
    }

    public void setAccounts(List<Account> accounts) {
        this.accounts = accounts;
    }

    public Department() {

    }

    public Department(String name, String address, Integer phone, List<Account> accounts) {

        this.name = name;
        this.address = address;
        this.phone = phone;
        this.accounts = accounts;
    }
}
