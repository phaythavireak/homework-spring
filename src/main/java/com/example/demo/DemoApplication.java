package com.example.demo;

import com.example.demo.ManyToMany.model.Students;
import com.example.demo.ManyToMany.model.Teacher;
import com.example.demo.ManyToMany.stduent_teacherentity.Student_TeacherEntity;
import com.example.demo.OneToManyAndManytoOne.entitiy.AccountEntity;
import com.example.demo.OneToManyAndManytoOne.model.Account;
import com.example.demo.OneToManyAndManytoOne.model.Department;
import com.example.demo.OneToOne.entitiy.StudentEntity;
import org.springframework.boot.ApplicationArguments;
import org.springframework.boot.ApplicationRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

import java.util.Collections;

@SpringBootApplication
public class DemoApplication implements ApplicationRunner {




    private StudentEntity studentEntity;
    private AccountEntity departmentsEntity;
    private Student_TeacherEntity student_teacherEntity;

    public DemoApplication(StudentEntity studentEntity, AccountEntity departmentsEntity, Student_TeacherEntity student_teacherEntity) {
        this.studentEntity = studentEntity;
        this.departmentsEntity = departmentsEntity;
        this.student_teacherEntity = student_teacherEntity;
    }

    @Override
    public void run(ApplicationArguments args) throws Exception {


//        Student student = new Student();
//        Course course = new Course();
//        course.setTitle("I love you");
//        course.setDescription("Sdach game");
//        student.setName("virak");
//        student.setAddress("PP");
//        student.setPhone(12345678);
//        student.setCourse(course);
//        studentEntity.save(student);
//
//
//        System.out.println(studentEntity.getall(student));



//        Department department = new Department();
//        Account account = new Account();
//        account.setAccount_name("ABA");
//        account.setAccount_num(324);
//        department.setName("virak");
//        department.setPhone(010474);
//        department.setAddress("CAM");
//        account.setDepartment(department);
//        department.setAccounts(Collections.singletonList(account));
//        departmentsEntity.insert(account);


        Students students = new Students();
        Teacher teacher = new Teacher();
        teacher.setName("sothearoth");
        teacher.setPhone_number(123);
        students.setStudent_name("minuth");
        students.setTeacher(Collections.singletonList(teacher));
        student_teacherEntity.insertes(students);


    }

    public static void main(String[] args) {
        SpringApplication.run(DemoApplication.class, args);
    }
}
